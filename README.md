# FALL attack repository

This is the source code for functional analysis attacks on logic locking
(FALL) attacks. The FALL attack is an attack on Secure Function Logic Locking
(SFLL) by Yasin et al. (CCS 2017) and TTLock (GLSVLSI 2017).  A notable
difference from past attacks (e.g., SAT/ATPG/AppSAT etc.) is that FALL
attacks **do not** require Oracle access to an unlocked circuit.

## Dependencies

This relies on the Python APIs that interface to the PicoSAT and
lingeling SAT Solvers. Please install these first:

1. PyPicoSAT: https://github.com/pysmt/pyPicoSAT
2. PyLingeling: https://github.com/pramodsu/pyLingeling

### Instructions for Installing PyPicoSAT

Step 1: Install SWIG. If you are running Ubuntu/Debian you can install SWIG
using apt-get.

    $ sudo apt-get install swig

If you are not running Ubuntu/Debian, consult the documentation for your
distribution's package manager. The SWIG website at http://www.swig.org/
also has installation instructions.

Step 2: Clone the pyPicoSAT repository.

    $ git clone https://github.com/pysmt/pyPicoSAT

Step 3: Run build.sh.

    $ ./build.sh picosat-965

Step 4: Run setup.py to install the pyPicoSAT library.

    $ python setup.py install --user

## Running the Code

The main file for the two attacks on SFLL is decrypt_unfied.py.

An example invocation is:

    $ python decrypt_unified.py ../benchmarks/locked/sfll_frac_4_limit_64/c2670_sfll_h_16_k_64_enc.bench --alg 3

As you can see the script accepts two arguments. The first is the bench file
containing the circuit to be attacked.  The second argument is the decryption
algorithm. Algorithm 1 is the SlidingWindow algorithm while Algorithm 3 is the
Distance2H algorithm described in the [paper](https://cse.iitk.ac.in/users/spramod/papers/date19.pdf).

The above command produces the following output:

    attempting to decrypt ../benchmarks/locked/sfll_frac_4_limit_64/c2670_sfll_h_16_k_64_enc.bench; with alg: 3
    [c2670_sfll_h_16_k_64_enc.bench/3] command_line: decrypt_unified.py ../benchmarks/locked/sfll_frac_4_limit_64/c2670_sfll_h_16_k_64_enc.bench --alg 3
    G1                   keyinputG1          
    G2                   keyinputG2          
    G3                   keyinputG3          
    G4                   keyinputG4          
    G5                   keyinputG5          
    G6                   keyinputG6          
    G7                   keyinputG7          
    G8                   keyinputG8          
    G11                  keyinputG11         
    G14                  keyinputG14         
    G15                  keyinputG15         
    G16                  keyinputG16         
    G19                  keyinputG19         
    G20                  keyinputG20         
    G21                  keyinputG21         
    G22                  keyinputG22         
    G23                  keyinputG23         
    G24                  keyinputG24         
    G25                  keyinputG25         
    G26                  keyinputG26         
    G27                  keyinputG27         
    G28                  keyinputG28         
    G29                  keyinputG29         
    G32                  keyinputG32         
    G33                  keyinputG33         
    G34                  keyinputG34         
    G35                  keyinputG35         
    G36                  keyinputG36         
    G37                  keyinputG37         
    G40                  keyinputG40         
    G43                  keyinputG43         
    G44                  keyinputG44         
    G47                  keyinputG47         
    G48                  keyinputG48         
    G49                  keyinputG49         
    G50                  keyinputG50         
    G51                  keyinputG51         
    G52                  keyinputG52         
    G53                  keyinputG53         
    G54                  keyinputG54         
    G55                  keyinputG55         
    G56                  keyinputG56         
    G57                  keyinputG57         
    G60                  keyinputG60         
    G61                  keyinputG61         
    G62                  keyinputG62         
    G63                  keyinputG63         
    G64                  keyinputG64         
    G65                  keyinputG65         
    G66                  keyinputG66         
    G67                  keyinputG67         
    G68                  keyinputG68         
    G69                  keyinputG69         
    G72                  keyinputG72         
    G73                  keyinputG73         
    G74                  keyinputG74         
    G75                  keyinputG75         
    G76                  keyinputG76         
    G77                  keyinputG77         
    G78                  keyinputG78         
    G79                  keyinputG79         
    G80                  keyinputG80         
    G81                  keyinputG81         
    G82                  keyinputG82         
    [c2670_sfll_h_16_k_64_enc.bench/3] found 64 comparators
    # of ckt inputs : 64
    # of gates      : 5864
    n4713 (  759/ 5864 @ 262)
    n4715 ( 1127/ 5864 @ 255)
    n4688 ( 1543/ 5864 @ 249)
    n4716 ( 2075/ 5864 @ 256)
    n4689 ( 2276/ 5864 @ 250)
    n4714 ( 2403/ 5864 @ 254)
    n4722 ( 2514/ 5864 @ 263)
    n4717 ( 2594/ 5864 @ 257)
    n4696 ( 2623/ 5864 @ 255)
    n4699 ( 3116/ 5864 @ 258)
    n4718 ( 3142/ 5864 @ 259)
    n4720 ( 3251/ 5864 @ 261)
    n4723 ( 3269/ 5864 @ 264)
    n4698 ( 3410/ 5864 @ 257)
    n4695 ( 3645/ 5864 @ 250)
    n4690 ( 4163/ 5864 @ 251)
    n4702 ( 4192/ 5864 @ 259)
    n4704 ( 4687/ 5864 @ 261)
    n4703 ( 5066/ 5864 @ 260)
    n4719 ( 5221/ 5864 @ 260)
    n4721 ( 5647/ 5864 @ 262)
    n4697 ( 5668/ 5864 @ 256)
    # of candidates : 22
    [c2670_sfll_h_16_k_64_enc.bench/3] found 22 candidates
    processing gate: n4713
    processing gate: n4715
    processing gate: n4688
    processing gate: n4716
    processing gate: n4689
    processing gate: n4714
    processing gate: n4722
    [1] FOUND key=0000010000010001110101100011011101110010101101110010011100101110
    processing gate: n4717
    processing gate: n4696
    processing gate: n4699
    processing gate: n4718
    processing gate: n4720
    processing gate: n4723
    [2] FOUND key=0000010000010001110101100011011101110010101101110010011100101110
    processing gate: n4698
    processing gate: n4695
    processing gate: n4690
    processing gate: n4702
    processing gate: n4704
    processing gate: n4703
    processing gate: n4719
    processing gate: n4721
    processing gate: n4697
    [c2670_sfll_h_16_k_64_enc.bench/3] found 1 key(s)
    key=0000010000010001110101100011011101110010101101110010011100101110
    [c2670_sfll_h_16_k_64_enc.bench/3] key=0000010000010001110101100011011101110010101101110010011100101110
    cpu_time 27.4

The first part of the output is showing the comparators that were identified.
The second part of the output is identifying the candidate nodes for cube
stripping.  Finally, the third part implements the key identification algorithm
(Distance2H or SlidingWindow).  The penultimate line of the output shows the
key(s) that was (were) found.

You can check the correctness of the key using lcmp tool developed by our
[SAT attack paper](https://bitbucket.org/spramod/host15-logic-encryption).

    $ lcmp ../benchmarks/original/c2670.bench ../benchmarks/locked/sfll_frac_4_limit_64/c2670_sfll_h_16_k_64_enc.bench key=0000010000010001110101100011011101110010101101110010011100101110
    equivalent

The output "equivalent" means that the key has been correctly identified.

# Contact

Pramod Subramanyan: spramod (AT) cse (DOT) iitk (DOT) ac (DOT) in

# References

If you use this code in a research paper, please cite the following [DATE 2019
paper](https://cse.iitk.ac.in/users/spramod/papers/date19.pdf).

* Deepak Sirone and Pramod Subramanyan.    
  Functional Analysis Attacks on Logic Locking.    
  Proceedings of Design Automation and Test in Europe (DATE) 2019.

